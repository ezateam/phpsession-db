<?php
// Routes

define('BASE', '/api/v1');

$app->get('/[{name}]', function ($request, $response, $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});

$app->get(BASE . '/session/all', function ($request, $response, $args) {

});

$app->get(BASE . '/session/[{id}]', function ($request, $response, $args) {
	$id = $args['id'];

	$count = $this->sessions->where('id', $id)->count();

	if ($count > 0) {

		$session = $this->sessions->where('id', $id)->first();

		$res = array(
			'status' => 'success',
			'id' => $session->id,
			'access' => $session->access,
			'data' => $session->data
		);

		$response->withJson($res);
	} else {
		$response->withJson(['status' => 'failure'], 404);
	}

});

$app->post(BASE . '/session/write', function ($request, $response, $args) {
	$post = $request->getParsedBody();

	$id = $post['id'];
	$access = time();
	$data = $post['data'];

	$count = $this->sessions->where('id', $id)->count();

	if ($count > 0) {
		$updated = $this->sessions->where('id', $id)
					   ->update(['access' => $access, 'data' => $data]);

		if (!$updated) {
			$response->withJson(['status' => 'failure'], 500);
		} else {
			$response->withJson(['status' => 'success']);
		}
	} else {
		$session = $this->sessions->create([
			'id' => $id,
			'access' => $access,
			'data' => $data
		]);

		if (!isset($session)) {
			$response->withJson(['status' => 'failure'], 500);
		} else {
			$response->withJson(['status' => 'success']);
		}
	}
	
});

$app->delete(BASE . '/session/destroy/[{id}]', function ($request, $response, $args) {

	$id = $args['id'];

	try {
		$session = $this->sessions->where('id', $id)->first();
		$deletedRows = $this->sessions->where('id', $id)->delete();
		$response->withJson(['status' => 'success', 'session' => $session]);
	} catch (\Exception $e) {
		$response->withJson(['status' => 'failure'], 500);
	}
});

$app->delete(BASE . '/session/clean/[{maxlifetime}]', function ($request, $response, $args) {
	
	$max = $args['maxlifetime'];
	$old = time() - $max;

	try {
		$sessions = $this->sessions->where('access', '<', $old)->get();
		$deletedRows = $this->sessions->where('access', '<', $old)->delete();
		$response->withJson(['status' => 'success', 'sessions' => $sessions]);
	} catch (\Exception $e) {
		$response->withJson(['status' => 'failure'], 500);
	}
});
