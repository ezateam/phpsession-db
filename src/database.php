<?php

use Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule;
$settings = require 'settings.php';
//$settings = $app->getContainer->get('settings')['db'];
$settings = $settings['settings']['db'];

$capsule->addConnection([
	'driver' => $settings['driver'],
	'host' => $settings['host'],
	'database' => $settings['name'],
	'username' => $settings['user'],
	'password' => $settings['pass'],
	'charset' => $settings['charset'],
	'collation' => $settings['collation'],
	'prefix' => $settings['prefix']
]);

$capsule->bootEloquent();