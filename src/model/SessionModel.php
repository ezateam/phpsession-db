<?php

/**
 * @author Zangue
 */

use Illuminate\Database\Eloquent\Model as Eloquent;

class SessionModel extends Eloquent {

	protected $table = 'phpsessions';

	protected $fillable = [
		'id',
		'access',
		'data'
	];
}